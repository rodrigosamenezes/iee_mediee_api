<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
	protected $table = 'groups';
    protected $fillable = ['id', 'user_id', 'name', 'description', 'type_of_measurer',
                           'activated', 'feedingtoken', 'readingtoken', 'has_update', 'last_activite'];
    protected $hidden = ['user_id'];
    protected $appends = array('status');
    public $timestamps = false;

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function measurer(){
    	return $this->belongsTo('App\TypeOfMeasurer', 'type_of_measurer', 'id');
    }

    public function mm02_3p_settings(){
    	return $this->hasOne('App\Mm02_3p_settings');
    }

    public function mm02_3p_measurers() {
    	return $this->hasMany('App\Mm02_3p_measurer');
    }

    public function notifiables() {
        return $this->hasMany('App\Notifiable');
    }

    public function getStatusAttribute() {
        if ($this->activated) {
            if ($this->last_activity) {
                $initialDate = strtotime($this->last_activity);
                $finalDate = time();
                //echo $finalDate . "\n\n";
                $difference = ($finalDate - $initialDate);
                //console.log(group.id, group.last_activity, difference);
                if ($difference > 15) {
                    return 'offline';
                }
                else if ($this->error) {
                    return 'error';
                }
                else {
                    return 'online';
                }
            }
            else {
                return 'offline';
            }
        }
        else {
            return 'desativated';
        }
    }

}

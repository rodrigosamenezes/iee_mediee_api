<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifiable extends Model
{
    protected $table = 'notifiables';
    protected $fillable = [
        'id',
        'group_id',
        'type',
        'destiny'
    ];
    public $timestamps = false;

    public function notificationQueueItems() {
        return $this->hasMany('App\NotificationQueueItem', 'notifiable_id', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mm02_3p_settings extends Model
{
	protected $table = 'mm02_3p_settings';
    protected $fillable = ['id', 'group_id', 'serial_port', 'bound_rate', 'delay'];
    protected $hidden = ['group_id'];
    public $timestamps = false;

    public function group() {
    	return $this->belongsTo('App\Group');
    }
}

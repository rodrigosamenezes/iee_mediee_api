<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mm02_3p_data extends Model
{
    protected $table = 'mm02_3p_data';
    protected $fillable = [
    	'id',
    	'mm02_3p_measurer_id',
    	'timestamp',
    	'v_a',
    	'v_b',
    	'v_c',
    	'i_a',
    	'i_b',
    	'i_c', 
    	'fpt', 'p_w', 'p_var', 'p_va', 'f', 'e_kwh', 'e_kvarh', 'e_kvah'];
    public $timestamps = false;

    public function mm02_3p_measurer(){
    	return $this->beloongsTo('App\Mm02_3p_measurer');
    }
}

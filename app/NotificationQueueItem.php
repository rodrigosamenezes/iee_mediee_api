<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationQueueItem extends Model
{
    protected $table = 'notification_queue';
    protected $fillable = [
        'id',
        'notifiable_id',
        'content',
        'created_at',
        'updated_at'
    ];
    protected $hidden = ['updated_at'];

    public function notifiable() {
        return $this->belongsTo('App\Notifiable', 'notifiable_id', 'id');
    }
}

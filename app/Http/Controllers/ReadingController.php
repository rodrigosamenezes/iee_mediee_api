<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\Mm02_3p_data;
use App\Mm02_3p_measurer;
use Illuminate\Support\Facades\DB;
use DateTime;

class ReadingController extends Controller
{
	public function info(Request $request){
		$readingtoken = $request->header('readingtoken');
		$group = Group::where('readingtoken', $readingtoken)->first(['id', 'has_update']);
		if($group){
			$group = Group::where('id', $group->id)->with(['mm02_3p_settings', 'mm02_3p_measurers'])
						->first(['id','name', 'type_of_measurer', 'activated', 'has_update', 'last_activity']);
			return ['ok' => true, 'data' => $group];
		}
		else {
			return ['ok' => false, 'message' => 'invalid reading token'];
		}
	}

	public function data(Request $request, $measurer_id) {
		$readingtoken = $request->header('readingtoken');
		$group = Group::where('readingtoken', $readingtoken)->first(['id', 'type_of_measurer']);
		if($group){
			if($group->type_of_measurer == 1){
				$measurer = Mm02_3p_measurer::find($measurer_id);
				if($measurer->group_id == $group->id){
					$data = DB::table('mm02_3p_data')->where('mm02_3p_measurer_id', $measurer_id)->paginate(5);
					return ['ok' => true, 'data' => $data];
				}
				else {
					return ['ok' => false, 'message' => 'not allowed'];
				}
			}
		}
		else {
			return ['ok' => false, 'message' => 'invalid reading token'];
		}
	}

	public function last(Request $request, $measurer_id, $normalized=false){
		$readingtoken = $request->header('readingtoken');
		$group = Group::where('readingtoken', $readingtoken)->first(['id', 'type_of_measurer', 'last_activity', 'activated', 'error']);
		if($group){
			if($group->type_of_measurer == 1){
				$measurer = Mm02_3p_measurer::find($measurer_id);
				if($measurer->group_id == $group->id){
					$data = DB::table('mm02_3p_data')->where('mm02_3p_measurer_id', $measurer_id)->orderBy('timestamp', 'desc')->first();
					$arData = (array) $data;
					$arData['group_status'] = $group->status;
					if($normalized){
						$dataN = $measurer->data()
						->where('timestamp', '>=', explode(" ", $arData['timestamp'])[0])
						->orderBy('timestamp', 'asc')->first();
						$arData['e_kwh'] -= $dataN->e_kwh;
						$arData['e_kvarh'] -= $dataN->e_kvarh;
						$arData['e_kvah'] -= $dataN->e_kvah;
						$this->debugData['t'] = $dataN;
					}
					return $this->returnOK($arData);
				}
				else {
					return ['ok' => false, 'message' => 'not allowed'];
				}
			}
		}
		else {
			return ['ok' => false, 'message' => 'invalid reading token'];
		}
	}

	public function view_measurer(Request $request, $measurer_id, $date_init, $date_end, $order, $resolution = null){
        $readingtoken = $request->header('readingtoken');
		$group = Group::where('readingtoken', $readingtoken)->first(['id', 'type_of_measurer', 'last_activity', 'activated', 'error']);
		if($group){
			if($group->type_of_measurer == 1){
	            $measurer = $group->mm02_3p_measurers()->where('id', $measurer_id)->first();
	            $data;
	            if($resolution){                    
	                $data = $measurer->data()
	                        ->where('timestamp', '>=', $date_init)
	                        ->where('timestamp', '<', $date_end)->orderBy('timestamp', $order)->get();
	                $data = $this->helper_set_results_resolution($data, $resolution);
	            }
	            else {
	                $data = $measurer->data()->where('timestamp', '>=', $date_init)
	                        ->where('timestamp', '<', $date_end)->orderBy('timestamp', $order)->paginate(15);
	            }
	            $measurerArray = $measurer->toArray();
	            $measurerArray['data'] = $data;
	            return ['ok' => true, 'data' => $measurerArray];
	        }
	    }
	    else {
			return ['ok' => false, 'message' => 'invalid reading token'];
		}
    }

    public function energy_days(Request $request, $measurer_id, $date_init, $amount_of_days){
    	$readingtoken = $request->header('readingtoken');
		$group = Group::where('readingtoken', $readingtoken)->first(['id', 'type_of_measurer', 'last_activity', 'activated', 'error']);
		if($group){
			if($group->type_of_measurer == 1){
				$measurer = $group->mm02_3p_measurers()->where('id', $measurer_id)->first();
				$answer = [];

				$date =  new DateTime($date_init);
				for($i = 0; $i < $amount_of_days; $i++){
				    $current_day = $date->format('Y-m-d');
				    $firstMeasurement = $measurer->data()
				    					->where('timestamp', '>=', $current_day . ' 00:00:00')
				    					->where('timestamp', '<=', $current_day . ' 24:59:59')
				    					->orderBy('timestamp', 'asc')->first();
				    $lastMeasurement = $measurer->data()
				    					->where('timestamp', '>=', $current_day . ' 00:00:00')
				    					->where('timestamp', '<=', $current_day . ' 24:59:59')
				    					->orderBy('timestamp', 'desc')->first();
				    if($firstMeasurement){
				    	$answer[] = [
				    		'day' => $current_day,
				    		'e_kwh' => $lastMeasurement->e_kwh - $firstMeasurement->e_kwh,
				    		'e_kvarh' => $lastMeasurement->e_kvarh - $firstMeasurement->e_kvarh
				    	];
				    }
				    else {
				    	$answer[] = [
				    		'day' => $current_day,
				    		'e_kwh' => 0,
				    		'e_kvarh' => 0
				    	];
				    }
				    $date->modify('+1 day');
				}
				return $this->returnOK($answer);
			}
		}
	    else {
			return ['ok' => false, 'message' => 'invalid reading token'];
		}
    }

    private function helper_set_results_resolution ($data, $resolution){
        $sizeOfData = sizeof($data);
        if($sizeOfData > $resolution){
            $newData = [];
            $newData[] = $data[0];

            $fator = ($sizeOfData)/($resolution - 1);
            for($i = 1; $i <= ($resolution - 2); $i++){
               $index = round($i*$fator);
               $newData[] = $data[$index];
            }

            $newData[] = $data[$sizeOfData -1];
            return $newData; 
        }
        else {
            return $data;
        }
    }


}

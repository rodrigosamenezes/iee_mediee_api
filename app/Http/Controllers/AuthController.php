<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Hash;
use Validator;
use App\User;
class AuthController extends Controller
{
    public function authenticate(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            'username' => 'required',
            'password' => 'required'
        ]);
        if($validator->fails()){
            return [
                'ok' => false,
                'message' => 'data is not valid',
                'errors' => $validator->errors()->all()
            ];
        }
        $usuario = User::where('username', $data['username'])->first();
        if($usuario && Hash::check($data['password'], $usuario->password)){
            $token = JWTAuth::fromUser($usuario);
            return response()->json([
                'ok' => true,
                'token' => $token,
            ]);
        }
        else {
            return [
                'ok' => false,
                'message' => 'invalid credentials'
            ];
        }
    }
}

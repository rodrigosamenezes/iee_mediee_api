<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\WelcomeNotifiable;
use App\Group;
use App\NotificationQueueItem;
use Illuminate\Support\Facades\Mail;

class JobsController extends Controller
{
    public function main(Request $request) {
        ignore_user_abort(true);
        $groups = Group::where('activated', true)
        ->where(function($query) {
            $query->where('error', null)
            ->orWhere('error', 'not like', 'offline%');
        })->get(['id', 'name', 'activated', 'last_activity', 'error']);
        foreach($groups as $group){
            if($group->status == 'offline'){
                $group->error = 'offline';
                $group->save();
                $notificationsContent = "Erro no grupo " . $group->id . " - " . $group->name . ".\n\nDescrição: Este grupo está offline.";
                foreach ($group->notifiables as $notifiable) {
                    $notificationQueueItem = new NotificationQueueItem(['content' => $notificationsContent]);
                    $notifiable->notificationQueueItems()->save($notificationQueueItem);
                }
            }
        }
        return 'ok';
    }

    public function notification_queue_email() {
        ignore_user_abort(true);
        $notificationItems = NotificationQueueItem::with('notifiable')->limit(10)->get();
        try{
            $idsToDelete = [];
            foreach($notificationItems as $item){   
                Mail::to($item->notifiable->destiny, 'Notificável')->send(new WelcomeNotifiable($item->content));
                $idsToDelete[] = $item->id;
            }
            NotificationQueueItem::destroy($idsToDelete);
            return sizeof($notificationItems);
        }
        catch (Exception $ex) {
            print_r($ex);
            return 'falha';
        }
    }

    
}

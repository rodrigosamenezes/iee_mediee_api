<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use Validator;
use App\Mm02_3p_data;
use Illuminate\Validation\Rule;
use App\NotificationQueueItem;

class FeedController extends Controller
{

	private $types = [
		'1' => [
			'settings' => 'mm02_3p_settings',
			'measurers' => 'mm02_3p_measurers'
		]
	];

	public function info(Request $request){
		$feedingtoken = $request->header('feedingtoken');
		$group = Group::where('feedingtoken', $feedingtoken)->first(['id', 'has_update']);
		if($group){
			$group->last_activity = date("Y-m-d H:i:s");
			if($group->has_update){
				$group->has_update = false;
			}
			$group->save();
			$group = Group::where('id', $group->id)->with(['mm02_3p_settings', 'mm02_3p_measurers'])
						->first(['id','name', 'type_of_measurer', 'activated', 'has_update']);
			return ['ok' => true, 'data' => $group];
		}
		else {
			return ['ok' => false, 'message' => 'invalid feeding token'];
		}
	}

	public function has_update(Request $request){
		$feedingtoken = $request->header('feedingtoken');
		$group = Group::where('feedingtoken', $feedingtoken)->first(['id', 'has_update']);
		if($group){
			$group->last_activity = date("Y-m-d H:i:s");
			$group->save();
			return ['ok' => true, 'data' => $group->has_update];
		}
		else {
			return ['ok' => false, 'message' => 'invalid feeding token'];
		}
	}

    public function feed(Request $request){
    	$feedingtoken = $request->header('feedingtoken');
    	if(!$feedingtoken){
    		return ['ok' => false, 'message' => 'bad request'];
    	}
    	
    	$group = Group::where('feedingtoken', $feedingtoken)->first(['id', 'activated', 'last_activity', 'error', 'has_update', 'type_of_measurer']);
    	if($group){
			if(substr($group->error, 0, strlen('offline')) === 'offline'){
				$notificationsContent = "Notificação grupo " . $group->id . " - " . $group->name . ".\n\nDescrição: Este grupo está ONLINE novamente.";
                foreach ($group->notifiables as $notifiable) {
                    $notificationQueueItem = new NotificationQueueItem(['content' => $notificationsContent]);
                    $notifiable->notificationQueueItems()->save($notificationQueueItem);
                }
			}
    		$group->last_activity = date("Y-m-d H:i:s");
    		$group->error = null;
    		$group->save();
    		if($group->has_update){
    			return ['ok' => false, 'code' => "update", 'message' => 'please, update your settings'];
    		}
    		if(!$group->activated){
    			return ['ok' => false, 'code' => "activity", 'message' => 'group not activated'];
    		}
    		if($group['type_of_measurer'] == '1'){
    			$measurers =  $group->mm02_3p_measurers()->where('activated', true)->get(['id']);
    			if(sizeof($measurers) == 0){
    				return ['ok' => false, 'code' => "activity", 'message' => 'there is none measurer activated'];
    			}
    			$measurersIds = array();
    			foreach ($measurers as $measurer){
    				$measurersIds[] =  $measurer->id;
    			}
    			$data = $request->except(['url']);
    			if(!is_array($data)) return ['ok' => false, 'code' => "invalid_data", 'message' => 'data must be array of measurements'];
    			if(sizeof($data) == 0) return ['ok' => false, 'code' => "invalid_data", 'message' => 'data must be a not empty array'];
    			$i = 0;
    			foreach($data as $dataRow){
	    			$validator = Validator::make($dataRow, [
	    				'mm02_3p_measurer_id' => ['required', 'numeric' ,'integer', Rule::in($measurersIds)],
	    				'timestamp' => 'required|date_format:"Y-m-d H:i:s"',
	    				'v_a' => 'required|numeric',
	    				'v_b' => 'required|numeric',
	    				'v_c' => 'required|numeric',
	    				'i_a' => 'required|numeric',
	    				'i_b' => 'required|numeric',
	    				'i_c' => 'required|numeric',
	    				'fpt' => 'sometimes|numeric',
	    				'p_w' => 'sometimes|numeric',
	    				'p_var' => 'sometimes|numeric',
	    				'p_va' => 'sometimes|numeric',
	    				'f'  => 'sometimes|numeric',
	    				'e_kwh' => 'sometimes|numeric',
	    				'e_kvarh' => 'sometimes|numeric',
	    				'e_kvah' => 'sometimes|numeric'
	    			]);
	    			if($validator->fails()){
	    				return ['ok' => false, 'message' => 'invalid data at ' . ($i + 1) . '# data row', 'errors' => $validator->errors()->all()];
	    			}
	    			$i++;
	    		}
	    		Mm02_3p_data::insert($data);
	    		return ['ok' => true];
    		}
    	}
    	else {
    		return ['ok' => false, 'message' => 'invalid feeding token'];	
    	}
    }

    public function setError(Request $request){
    	$feedingtoken = $request->header('feedingtoken');
    	if(!$feedingtoken){
    		return ['ok' => false, 'message' => 'bad request'];
    	}

		$error = $request->only('error');
		if(!$error['error']){
			return ['ok' => false, 'message' => 'bad request'];
		}

		$group = Group::with('notifiables')->where('feedingtoken', $feedingtoken)->first();
		if($group){
			$group->last_activity = date("Y-m-d H:i:s");
			$group->error = 'error: ' . $error['error'];
			$group->save();
			$notificationsContent = "Erro no grupo " . $group->id . " - " . $group->name . ".\n\nDescrição: " . $error['error'];
			foreach ($group->notifiables as $notifiable) {
				$notificationQueueItem = new NotificationQueueItem(['content' => $notificationsContent]);
				$notifiable->notificationQueueItems()->save($notificationQueueItem);
			}
			return ['ok' => true, 'group' => $group];
		}
		else {
			return ['ok' => false, 'message' => 'invalid feeding token'];
		}
    }
}

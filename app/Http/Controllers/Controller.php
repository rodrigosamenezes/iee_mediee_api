<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $debugData = [];
    protected function returnOK($data){
    	$ret = ['ok' => true, 'data' => $data];
    	if ($this->debugData){
    		$ret['debug'] = $this->debugData;
    	}
    	return $ret;
    }
}

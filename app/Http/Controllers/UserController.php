<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\User;

class UserController extends Controller
{
    public function create_first_user(){
    	$user = new User([
    		'username' => 'test',
    		'password' => Hash::make('test'),
    		'name' => 'Test',
    		'description' => 'Test User to test'
    		]);
    	$user->save();
    	return $user;
    }

}

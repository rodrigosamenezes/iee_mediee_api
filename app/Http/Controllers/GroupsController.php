<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Group;
use Validator;
use Illuminate\Validation\Rule;
use App\TypeOfMeasurer;
use Hash;
use App\Notifiable;


class GroupsController extends Controller {
    public function __construct(){
    	$this->middleware('jwt.auth');
    }

    public function all(){
    	$groups = Group::where('user_id', Auth::user()->id)->with('measurer:id,name')->get();
    	return ['ok' => true, 'data' => $groups];
    }

    public function allActivities(){
        $groups = Group::where('user_id', Auth::user()->id)->where('activated', true)->get(['id', 'error', 'last_activity', 'activated']);
        return ['ok' => true, 'data' => $groups];
    }

    public function create(Request $request){
    	$typesOfMeasurers = TypeOfMeasurer::all(['id']);
    	$idsOfTypesOfMeasurers = array();
    	foreach($typesOfMeasurers as $type){
    		$idsOfTypesOfMeasurers[] = $type->id;
    	}
		$newGroupData = $request->only('name', 'description', 'type_of_measurer');
    	$validator = Validator::make($newGroupData, [
    		'name' => 'required',
    		'description' => 'required',
    		'type_of_measurer' => ['required', Rule::in($idsOfTypesOfMeasurers)],
    	]);
    	if($validator->fails()){
    		return ['ok' => false, 'message' => 'Invalid data.', 'errors' => $validator->errors()->all()];
    	}
    	$settingsData;
    	$measurersData;
    	if($newGroupData['type_of_measurer'] == 1) { //MM02_3P
    		$settingsData = $request->only('mm02_3p_settings');
    		$validator = Validator::make($settingsData, [
    			'mm02_3p_settings' => 'required',
    			'mm02_3p_settings.serial_port' => 'required',
    			'mm02_3p_settings.bound_rate' => 'required',
    			'mm02_3p_settings.delay' => 'required'
    		]);

    		if($validator->fails()){
    			return ['ok' => false, 'message' => 'Invalid data.', 'errors' => $validator->errors()->all()];
    		}

    		$measurersData = $request->only('mm02_3p_measurers');
    		$validator =  Validator::make($measurersData, [
    			'mm02_3p_measurers' => 'required|array'
    		]);

    		if($validator->fails()){
    			return ['ok' => false, 'message' => 'Invalid data.', 'errors' => $validator->errors()->all()];
    		}

    		if(sizeof($measurersData['mm02_3p_measurers']) < 1){
    			return ['ok' => false, 'message' => 'Invalid data.', 'errors' => 'mm02_3p_measurers array cannot be empty.'];
			}

			for ($i =  0; $i < sizeof($measurersData['mm02_3p_measurers']); $i++){
				$measurerData = $measurersData['mm02_3p_measurers'][$i];
				$validator = Validator::make($measurerData,[
					'name' => 'required',
					'modbus_id' => 'required',
					'activated' => 'required'
				]);
				if($validator->fails()){
					return ['ok' => false, 'message' => 'Invalid data at measurer ' . $i, 'errors' => $validator->errors()->all()];
				}
			}
			$newGroupData['activated'] = false;
			$newGroupData['feedingtoken'] = md5(date_timestamp_get(date_create()) . rand());
            $newGroupData['readingtoken'] = md5(date_timestamp_get(date_create()) . rand());
			$newGroupData['has_update'] = true;

			$group = Auth::user()->groups()->create($newGroupData);
			$group->mm02_3p_settings()->create($settingsData['mm02_3p_settings']);
			$group->mm02_3p_measurers()->createMany($measurersData['mm02_3p_measurers']);
			return ['ok' => true];

    	}
    }

    public function update(Request $request, $id = null){
    	$fromBase = $this->view($id);
    	if(!$fromBase['ok'])
    		return $fromBase;
    	$groupFromBase = $fromBase['data'];
    	
		$newGroupData = $request->only('name', 'description');
    	$validator = Validator::make($newGroupData, [
    		'name' => 'required',
    		'description' => 'required'
    	]);
    	if($validator->fails()){
    		return ['ok' => false, 'message' => 'Invalid data.', 'errors' => $validator->errors()->all()];
    	}
    	$settingsData;
    	$measurersData;
    	if($groupFromBase->type_of_measurer == 1) { //MM02_3P
    		$settingsData = $request->only('mm02_3p_settings');
    		$validator = Validator::make($settingsData, [
    			'mm02_3p_settings' => 'required',
    			'mm02_3p_settings.serial_port' => 'required',
    			'mm02_3p_settings.bound_rate' => 'required',
    			'mm02_3p_settings.delay' => 'required'
    		]);

    		if($validator->fails()){
    			return ['ok' => false, 'message' => 'Invalid data.', 'errors' => $validator->errors()->all()];
    		}

    		$measurersData = $request->only('mm02_3p_measurers');
    		$validator =  Validator::make($measurersData, [
    			'mm02_3p_measurers' => 'required|array'
    		]);

    		if($validator->fails()){
    			return ['ok' => false, 'message' => 'Invalid data.', 'errors' => $validator->errors()->all()];
    		}

    		if(sizeof($measurersData['mm02_3p_measurers']) < sizeof($groupFromBase->mm02_3p_measurers)){
    			return ['ok' => false, 'message' => 'Invalid data.', 'errors' => 'mm02_3p_measurers array cannot be empty.'];
			}

			for ($i =  0; $i < sizeof($measurersData['mm02_3p_measurers']); $i++){
				$measurerData = $measurersData['mm02_3p_measurers'][$i];
				$validator = Validator::make($measurerData,[
					'name' => 'required',
					'modbus_id' => 'required',
					'activated' => 'required'
				]);
				if($validator->fails()){
					return ['ok' => false, 'message' => 'Invalid data at measurer ' . $i, 'errors' => $validator->errors()->all()];
				}
			}
			$newGroupData['has_update'] = true;

			$groupFromBase->fill($newGroupData);
			$groupFromBase->mm02_3p_settings->fill($settingsData['mm02_3p_settings']);
			for($i = 0; $i < sizeof($groupFromBase->mm02_3p_measurers); $i++){
				$groupFromBase->mm02_3p_measurers[$i]->fill($measurersData['mm02_3p_measurers'][$i]);
				$groupFromBase->mm02_3p_measurers[$i]->save();
			}
            for($i = sizeof($groupFromBase->mm02_3p_measurers); $i < sizeof($measurersData['mm02_3p_measurers']); $i++){
                $groupFromBase->mm02_3p_measurers()->create($measurersData['mm02_3p_measurers'][$i]);
            }
			$groupFromBase->mm02_3p_settings->save();
			$groupFromBase->save();
			return ['ok' => true];
    	}
    }

	public function view($group_id){
		$group = Group::find($group_id);
		if($group->user_id != Auth::user()->id){
			return ['ok' => false, 'message' => 'not authorized'];
		}
		if($group->type_of_measurer == 1){
			$group = Group::where('id', $group_id)->with(['mm02_3p_settings', 'mm02_3p_measurers', 'measurer', 'notifiables'])->first();	
		}
		
		return ['ok' => true, 'data' => $group];
	}

    public function view_measurer($group_id, $measurer_id, $date_init, $date_end, $order, $resolution = null){
        $group = Group::find($group_id);
        if(!$group || $group->user_id != Auth::user()->id){
            return ['ok' => false, 'message' => 'not authorized'];
        }
        if($group->type_of_measurer == 1){
            $measurer = $group->mm02_3p_measurers()->where('id', $measurer_id)->first();
            $data;
            if($resolution){                    
                $data = $measurer->data()
                        ->where('timestamp', '>=', $date_init)
                        ->where('timestamp', '<', $date_end)->orderBy('timestamp', $order)->get();
                $data = $this->helper_set_results_resolution($data, $resolution);
            }
            else {
                $data = $measurer->data()->where('timestamp', '>=', $date_init)
                        ->where('timestamp', '<', $date_end)->orderBy('timestamp', $order)->paginate(15);
            }
            $measurerArray = $measurer->toArray();
            $measurerArray['data'] = $data;
            return ['ok' => true, 'data' => $measurerArray];
        }
    }

	public function turnon($id){
		$group = Group::find($id);
		if($group->user_id != Auth::user()->id){
			return ['ok' => false, 'message' => 'not authorized'];
		}
		if(!$group->activated){
			$group->activated = true;
			$group->has_update = true;
			$group->save();
		}
		return ['ok' => true];
	}

	public function turnoff($id){
		$group = Group::find($id);
		if($group->user_id != Auth::user()->id){
			return ['ok' => false, 'message' => 'not authorized'];
		}
		if($group->activated){
			$group->activated = false;
			$group->has_update = true;
			$group->save();
		}
		return ['ok' => true];
	}

	public function notifiables_getAll($group_id){
		$group = Group::find($group_id);
		if($group->user_id != Auth::user()->id){
			return ['ok' => false, 'message' => 'not authorized'];
		}
		return ['ok' => true, 'data' => $group->notifiables()->get()];
	}

	public function notifiables_add(Request $request, $group_id) {
		$group = Group::find($group_id);
		if($group->user_id != Auth::user()->id){
			return ['ok' => false, 'message' => 'not authorized'];
		}

		$newNotifiableData = $request->all();
		$validator = Validator::make($newNotifiableData, [
			'type' => 'required',
			'destiny' => 'required',
		]);

		if($validator->fails()){
			return ['ok' => false, 'message' => 'Invalid data.', 'errors' => $validator->errors()->all()];
		}

		$group->notifiables()->create($newNotifiableData);
		return ['ok' => true];
	}

	public function notifiables_remove(Request $request, $group_id){
		$notifiable_id = $request->only('notifiable_id');
		$group = Group::find($group_id);
		if($group->user_id != Auth::user()->id){
			return ['ok' => false, 'message' => 'not authorized'];
		}
		$notifiable = $group->notifiables()->find($notifiable_id);
		//print_r($notifiable);
		if($notifiable == null) {
			return ['ok' => false, 'message' => 'not found'];
		}

		$notifiable[0]->delete();
		return ['ok' => true];
		
	}

    private function helper_set_results_resolution ($data, $resolution){
        $sizeOfData = sizeof($data);
        if($sizeOfData > $resolution){
            $newData = [];
            $newData[] = $data[0];

            $fator = $sizeOfData/($resolution - 1);
            for($i = 1; $i <= ($resolution - 2); $i++){
               $index = round($i*$fator);
               $newData[] = $data[$index];
            }

            $newData[] = $data[$sizeOfData -1];
            return $newData; 
        }
        else {
            return $data;
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeOfMeasurer extends Model
{
	protected $table = 'types_of_measurers';
    protected $fillable = ['id', 'name', 'description'];
    public $timestamps = false;

    public function groups(){
    	return $this->hasMany('App\Group');
    }
}

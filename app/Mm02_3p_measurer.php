<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mm02_3p_measurer extends Model
{
	protected $table = 'mm02_3p_measurers';
    protected $fillable = ['id', 'group_id', 'name', 'modbus_id', 'activated', 'error'];
    protected $hidden = ['group_id'];
    public $timestamps = false;

    public function data(){
    	return $this->hasMany('App\Mm02_3p_data');
    }
    public function group(){
    	return $this->belongsTo('App\Group');
    }
}

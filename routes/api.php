<?php

header('Access-Control-Allow-Origin: *'); 
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, PATCH');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , authorization, Feedingtoken, readingtoken');

Route::get('/', function() { return ['ok' => true, 'message' => 'hello from mediee api.'];});
Route::post('/authenticate', 'AuthController@authenticate');

Route::get('/groups', 'GroupsController@all');
Route::post('/groups', 'GroupsController@create');
Route::put('/groups/{id}', 'GroupsController@update');
Route::get('/groups/{id}', 'GroupsController@view');
Route::get('/groups/{id}/{measurer_id}/{date_init}/{date_end}/{order}/{resolution?}', 'GroupsController@view_measurer');
Route::put('/groups/{id}/turnon', 'GroupsController@turnon');
Route::put('/groups/{id}/turnoff', 'GroupsController@turnoff');
Route::get('/groups/all_activities/get', 'GroupsController@allActivities');

Route::get('/notifiables/{group_id}', 'GroupsController@notifiables_getAll');
Route::post('/notifiables/{group_id}', 'GroupsController@notifiables_add');
Route::delete('/notifiables/{group_id}', 'GroupsController@notifiables_remove');


Route::get('/feed', 'FeedController@info');
Route::post('/feed', 'FeedController@feed');
Route::get('/feed/has_update', 'FeedController@has_update');
Route::post('/feed/seterror', 'FeedController@setError');

Route::get('/read', 'ReadingController@info');
Route::options('/read', function() {
	return '';
});
Route::get('/read/info', 'ReadingController@info');
Route::get('/read/{measurer_id}', 'ReadingController@data');
Route::get('/read/{measurer_id}/last/{normalized?}', 'ReadingController@last');
Route::get('/read/view_measurer/{measurer_id}/{date_init}/{date_end}/{order}/{resolution?}', 'ReadingController@view_measurer');
Route::get('/read/energy/{measurer_id}/{date_init}/{amount_of_days}', 'ReadingController@energy_days');

Route::get('/cronjobs/main', 'JobsController@main');
Route::get('/cronjobs/notification/email', 'JobsController@notification_queue_email'); 
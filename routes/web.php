<?php

header('Access-Control-Allow-Origin: *'); 
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, PATCH');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization, Feedingtoken, readingtoken');

//Route::get('/', function() { return ['ok' => true, 'message' => 'hello from mediee api.'];});
Route::post('/authenticate', 'AuthController@authenticate');

Route::get('/groups', 'GroupsController@all');
Route::post('/groups', 'GroupsController@create');
Route::put('/groups/{id}', 'GroupsController@update');
Route::get('/groups/{id}', 'GroupsController@view');
Route::put('/groups/{id}/turnon', 'GroupsController@turnon');
Route::put('/groups/{id}/turnoff', 'GroupsController@turnoff');
Route::get('/groups/{id}/notifiables', 'GroupsController@notifiables_getAll');
Route::post('/groups/{id}/notifiables', 'GroupsController@notifiables_add');
Route::delete('/groups/{id}/notifiables/{$notifiable_id}', 'GroupsController@notifiables_remove');
Route::get('/groups/all_activities/get', 'GroupsController@allActivities');

Route::get('/feed', 'FeedController@info');
Route::post('/feed', 'FeedController@feed');
Route::get('/feed/has_update', 'FeedController@has_update');
Route::post('/feed/seterror', 'FeedController@setError');

Route::get('/read', 'ReadingController@info');
Route::get('/read/{measurer_id}', 'ReadingController@data');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function(){
	return view('login');
});
//Route::get('/create_test_user', 'UserController@create_first_user');


Route::get('/cronjobs/main', 'JobsController@main'); 
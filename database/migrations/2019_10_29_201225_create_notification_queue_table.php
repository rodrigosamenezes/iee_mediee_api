<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_queue', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('notifiable_id')->unsigned();
            $table->string('content');
            $table->timestamps();
            $table->foreign('notifiable_id')->references('id')->on('notifiables');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_queue');
    }
}

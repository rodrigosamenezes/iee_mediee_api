<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->string('description');
            $table->integer('type_of_measurer')->unsigned();
            $table->boolean('activated');
            $table->string('feedingtoken');
            $table->string('readingtoken');
            $table->boolean('has_update');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('type_of_measurer')->references('id')->on('types_of_measurers');
            $table->string('error')->nullable();
            $table->timestamp('last_activity')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMm02dataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mm02_3p_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mm02_3p_measurer_id')->unsigned();
            $table->timestamp('timestamp');
            $table->float('v_a')->nullable();
            $table->float('v_b')->nullable();
            $table->float('v_c')->nullable();
            $table->float('i_a')->nullable();
            $table->float('i_b')->nullable();
            $table->float('i_c')->nullable();
            $table->float('fpt')->nullable();
            $table->float('p_w')->nullable();
            $table->float('p_var')->nullable();
            $table->float('p_va')->nullable();
            $table->float('f')->nullable();
            $table->float('e_kwh')->nullable();
            $table->float('e_kvarh')->nullable();
            $table->float('e_kvah')->nullable();

            $table->foreign('mm02_3p_measurer_id')->references('id')->on('mm02_3p_measurers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mm02_data');
    }
}

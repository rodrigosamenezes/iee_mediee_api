<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'username' => 'test',
    		'password' => Hash::make('test'),
    		'name' => 'Test',
    		'description' => 'Test User to test'
        ]);

        DB::table('types_of_measurers')->insert([
        	'name' => 'MM02_3P',
        	'description' => 'MM02 trifásico.'
        ]);
    }
}
